/*
 * controlConsumerMain.cpp
 *
 *  Created on: 09.03.2020
 *      Author: martin
 */

#include <iostream>

#include "SDCLib/Data/SDC/SDCProvider.h"

#include "SDCLib/SDCLibrary.h"
#include "SDCLib/SDCInstance.h"

#include "SDCLib/Data/SDC/SDCProviderComponentStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderMDStateHandler.h"
#include "SDCLib/Data/SDC/MDIB/ChannelDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/CodedValue.h"
#include "SDCLib/Data/SDC/MDIB/SimpleTypesMapping.h"
#include "SDCLib/Data/SDC/MDIB/MdsDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/LocalizedText.h"
#include "SDCLib/Data/SDC/MDIB/MdDescription.h"
#include "SDCLib/Data/SDC/MDIB/MetricQuality.h"
#include "SDCLib/Data/SDC/MDIB/Range.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricState.h"
#include "SDCLib/Data/SDC/MDIB/SampleArrayValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricState.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricState.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/SystemContextDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/MetaData.h"
#include "SDCLib/Dev/DeviceCharacteristics.h"
#include "SDCLib/Data/SDC/MDIB/VmdDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/custom/OperationInvocationContext.h"
#include "SDCLib/Data/SDC/SDCProviderActivateOperationHandler.h"
#include "SDCLib/Data/SDC/MDIB/ActivateOperationDescriptor.h"
#include "SDCLib/Data/SDC/FutureInvocationState.h"

#include "SDCLib/Data/SDC/SDCConsumer.h"
#include "SDCLib/Data/SDC/SDCConsumerConnectionLostHandler.h"
#include "SDCLib/Data/SDC/SDCConsumerMDStateHandler.h"
#include "OSELib/SDC/ServiceManager.h"

#include "SDCLib/Util/DebugOut.h"
#include "SDCLib/Util/Task.h"

#include "../util/LatencyMeasurement.h"

#include <MyConstants.h>
#include "../sharedClasses/ControlledDeviceCounterEventHandler.h"
#include "../util/Util.h"

using namespace SDCLib;
using namespace SDCLib::Util;
using namespace SDCLib::Data::SDC;

std::string deviceEPR("urn:uuid:2299249a-0167-5dde-9acf-000000000000");

const std::string HANDLE_CONTROLLED_DEV_COMMAND("h_act");

//@see: https://www.modernescpp.com/index.php/c-core-guidelines-be-aware-of-the-traps-of-condition-variables
std::condition_variable m_receivedEventCondVar; // trigger to signal that the event was received
std::mutex m_receivedEventMutex; // corresponding mutex for the conditional variable
std::atomic<bool> m_receivedEventBoolean{false}; // additional flag to signal the received data

int main(int argc, char* argv[])
{

	if(argc != 4){
		std::cerr << "wrong number of arguments: " << argc << std::endl;
		std::cerr << "usage: ./controlConsumer < EPRofControlledProvider > < #ofRuns > < intervalBetweenTriggersInMS >" << std::endl;
		return -1;
	}

	deviceEPR = argv[1];
	int numberOfRuns = atoi(argv[2]);
	int interval = atoi(argv[3]);

	LatencyMeasurement::getInstance().clearAllMeasurements();

	auto now = std::chrono::high_resolution_clock::now();
	auto nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()).count();

	std::list<long long> startTimestamps;
	std::list<long long> stopTimestamps;

	// Startup
	DebugOut(DebugOut::Default, "Control Consumer") << "Startup" << std::endl;
	SDCLibrary::getInstance().startup(OSELib::LogLevel::Error);
	DebugOut::DEBUG_LEVEL = DebugOut::Silent; //configure log level


	// create the consumer
	auto t_SDCInstanceConsumer = std::make_shared<SDCInstance>(true);
	// Some restriction
	t_SDCInstanceConsumer->setIP6enabled(false);
	t_SDCInstanceConsumer->setIP4enabled(true);
	// Bind it to interface that matches the internal criteria (usually the first enumerated)
	if(!t_SDCInstanceConsumer->bindToDefaultNetworkInterface()) {
		std::cout << "Failed to bind to default network interface! Exit..." << std::endl;
		return -1;
	}

	OSELib::SDC::ServiceManager t_serviceManager(t_SDCInstanceConsumer);

	// build the EPR of the corresponding controlled device
	std::string controlledDeviceEpr = deviceEPR;

	auto t_consumer(t_serviceManager.discoverEndpointReference(controlledDeviceEpr));

	if (t_consumer != nullptr) {
		std::cout << "Found controlled device with EPR: " << t_consumer->getEndpointReference() << std::endl;

		auto teh_controlledDevCounterMetric =
				std::make_shared<ControlledDeviceCounterEventHandler>(HANDLE_CONTROLLED_DEV_COUNTER_METRIC, &stopTimestamps,
					m_receivedEventCondVar,
					m_receivedEventBoolean);
		t_consumer->registerStateEventHandler(teh_controlledDevCounterMetric.get());

		waitStartup();

		for ( int i = 0; i < numberOfRuns; i ++ )
		{
			FutureInvocationState fis;

			// take the timestamp
			auto nowTS = std::chrono::steady_clock::now();
			long long nanosTS = std::chrono::duration_cast<std::chrono::nanoseconds>(nowTS.time_since_epoch()).count();
			startTimestamps.push_back(nanosTS);

			// trigger activate operation
			t_consumer->activate(HANDLE_CONTROLLED_DEV_COMMAND, fis);
			//as we use the blocking waiting for the numeric value change, we do not need this wait here. More precisely, it would not work here, as the numeric value change happens before publishing the "Fin"
//			Util::DebugOut(Util::DebugOut::Default, "DOC") << "Commit ActivateOperation state: " << fis.waitReceived(InvocationState::Fin, 2000);

			// Block here
//			std::cout<< "blocking" << std::endl;
			std::unique_lock<std::mutex> uLock(m_receivedEventMutex);
			m_receivedEventCondVar.wait(uLock, [&]{return m_receivedEventBoolean.load();});
			m_receivedEventBoolean.store(false); //reset flag
//			std::cout<< "blocking released" << std::endl;

			// sleep a certain time before triggering the next value change
			std::this_thread::sleep_for(std::chrono::milliseconds(interval));

		}

		for (auto &elem : startTimestamps) {
			std::cout << elem << std::endl;
		}

		LatencyMeasurement::getInstance().setT0List(startTimestamps);
		LatencyMeasurement::getInstance().setT1List(stopTimestamps);
		LatencyMeasurement::getInstance().writeEveryMeasurementToFiles(true);
	}
	else {
		std::cerr << "Did NOT find a corresponding controlled device with EPR " << controlledDeviceEpr << std::endl;
	}
}

