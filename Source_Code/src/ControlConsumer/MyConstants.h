/*
 * MyConstants.h
 *
 *  Created on: 11.11.2019
 *      Author: martin
 */

#ifndef SRC_CONTROLPROVIDER_MYCONSTANTS_H_
#define SRC_CONTROLPROVIDER_MYCONSTANTS_H_

#include <string>

const std::string CLASS_NAME("SdcControlProvider");
const std::string HANDLE_CONTROL_INDICATOR_METRIC("h_indicator");
const std::string HANDLE_CONTROLLED_DEV_COUNTER_METRIC("h_counter");
const std::string HANDLE_COMMAND("h_act_start");

class MyConstants {
public:
	MyConstants();
	virtual ~MyConstants();
};

#endif /* SRC_CONTROLPROVIDER_MYCONSTANTS_H_ */
