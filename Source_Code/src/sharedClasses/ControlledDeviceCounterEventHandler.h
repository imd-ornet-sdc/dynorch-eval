/*
 * ControlledDeviceCounterEventHandler.h
 *
 *  Created on: 09.03.2020
 *      Author: martin
 */

#include <iostream>

#include "SDCLib/Data/SDC/SDCProvider.h"

#include "SDCLib/SDCLibrary.h"
#include "SDCLib/SDCInstance.h"

#include "SDCLib/Data/SDC/SDCProviderComponentStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderMDStateHandler.h"
#include "SDCLib/Data/SDC/MDIB/ChannelDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/CodedValue.h"
#include "SDCLib/Data/SDC/MDIB/SimpleTypesMapping.h"
#include "SDCLib/Data/SDC/MDIB/MdsDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/LocalizedText.h"
#include "SDCLib/Data/SDC/MDIB/MdDescription.h"
#include "SDCLib/Data/SDC/MDIB/MetricQuality.h"
#include "SDCLib/Data/SDC/MDIB/Range.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricState.h"
#include "SDCLib/Data/SDC/MDIB/SampleArrayValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricState.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricState.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/SystemContextDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/MetaData.h"
#include "SDCLib/Dev/DeviceCharacteristics.h"
#include "SDCLib/Data/SDC/MDIB/VmdDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/custom/OperationInvocationContext.h"
#include "SDCLib/Data/SDC/SDCProviderActivateOperationHandler.h"
#include "SDCLib/Data/SDC/MDIB/ActivateOperationDescriptor.h"

#include "SDCLib/Data/SDC/SDCConsumer.h"
#include "SDCLib/Data/SDC/SDCConsumerConnectionLostHandler.h"
#include "SDCLib/Data/SDC/SDCConsumerMDStateHandler.h"
#include "OSELib/SDC/ServiceManager.h"

#include "SDCLib/Util/DebugOut.h"
#include "SDCLib/Util/Task.h"

#include <mutex>
#include <condition_variable>

#include "../util/LatencyMeasurement.h"


using namespace SDCLib;
using namespace SDCLib::Util;
using namespace SDCLib::Data::SDC;

/**
 * This class is responsible for handle events coming from the controlled device
 */
class ControlledDeviceCounterEventHandler : public SDCConsumerMDStateHandler<NumericMetricState>
{
private:

	std::list<long long> *m_finishTimestamps;

	//@see: https://www.modernescpp.com/index.php/c-core-guidelines-be-aware-of-the-traps-of-condition-variables
	std::condition_variable& m_receivedEventCondVar; // trigger to signal that the event was received
	std::atomic<bool>& m_receivedEventBoolean; // additional flag to signal the received data

public:
    ControlledDeviceCounterEventHandler(const std::string & handle, std::list<long long> *p_stopTimestamps,
    		std::condition_variable& p_receivedEventCondVar,
			std::atomic<bool>& p_receivedEventBoolean) :
    	SDCConsumerMDStateHandler(handle),
		m_finishTimestamps(p_stopTimestamps),
		m_receivedEventCondVar(p_receivedEventCondVar),
		m_receivedEventBoolean(p_receivedEventBoolean)
	{
	}

    /**
     * when the metric changes its value, we know that the trigger from the control device (the
     * provider part of this implementation) was successfully handled. Thus, we can take the
     * "finished" timestamp.
     */
    void onStateChanged(const NumericMetricState & state) override
    {
    	// take the timestamp
		auto nowTF = std::chrono::steady_clock::now();
		long long nanosTF = std::chrono::duration_cast<std::chrono::nanoseconds>(nowTF.time_since_epoch()).count();
		m_finishTimestamps->push_back(nanosTF);

    	DebugOut(DebugOut::Default, "Control Provider") << "Consumer: Received state change from controlled Device of descriptor: " << this->getDescriptorHandle() << ". New value is: " << state.getMetricValue().getValue() << std::endl;

    	//@see: https://www.modernescpp.com/index.php/c-core-guidelines-be-aware-of-the-traps-of-condition-variables
    	m_receivedEventBoolean = true;
    	m_receivedEventCondVar.notify_one();
    }

    void onOperationInvoked(const OperationInvocationContext & oic, InvocationState is) override
    {
        DebugOut(DebugOut::Default, "Control Provider") << "Consumer: Received operation invoked (numeric metric) (ID, STATE) of " << this->getDescriptorHandle() << ": " << oic.transactionId << ", " << Data::SDC::EnumToString::convert(is) << std::endl;
    }
};
