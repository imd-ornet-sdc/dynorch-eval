#include "SDCLib/SDCLibrary.h"
#include "SDCLib/Data/SDC/SDCConsumer.h"
#include "SDCLib/Data/SDC/SDCConsumerConnectionLostHandler.h"
#include "SDCLib/Data/SDC/SDCConsumerMDStateHandler.h"
#include "SDCLib/Data/SDC/MDIB/MdsDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/MetricQuality.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricState.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricState.h"
#include "SDCLib/Data/SDC/MDIB/SampleArrayValue.h"
#include "SDCLib/Data/SDC/MDIB/LocationContextState.h"
#include "SDCLib/Data/SDC/MDIB/LocationDetail.h"
#include "SDCLib/Data/SDC/MDIB/AlertSignalState.h"
#include "SDCLib/Data/SDC/MDIB/custom/OperationInvocationContext.h"
#include "SDCLib/Data/SDC/FutureInvocationState.h"
#include "SDCLib/Util/DebugOut.h"

#include <stdlib.h>

#include "OSELib/SDC/ServiceManager.h"

#include "EprHandling.h"
#include "../util/Util.h"

using namespace SDCLib;
using namespace SDCLib::Util;
using namespace SDCLib::Data::SDC;

const std::string HANDLE_CONTROLLED_DEV_COUNTER_METRIC("h_counter");
const std::string HANDLE_CONTROLLED_DEV_COMMAND("h_act");
const std::string HANDLE_CONTROL_INDICATOR_METRIC("h_indicator");
const std::string HANDLE_CONTROL_COMMAND_START("h_act_start");

class ControlEventHandler;

class ControlConsumerWrapper
{
private:

	std::unique_ptr<SDCLib::Data::SDC::SDCConsumer> m_control_consumer = nullptr;
	std::unique_ptr<SDCLib::Data::SDC::SDCConsumer> m_controlled_consumer = nullptr;


public:

	ControlConsumerWrapper(std::unique_ptr<SDCLib::Data::SDC::SDCConsumer> p_control_consumer)
	: m_control_consumer(std::move(p_control_consumer))
	{
	}

	bool activate()
	{
		assert(nullptr != m_controlled_consumer);
		if ( nullptr != m_controlled_consumer ) {
			FutureInvocationState fis;
			m_controlled_consumer->activate(HANDLE_CONTROLLED_DEV_COMMAND, fis);
			Util::DebugOut(Util::DebugOut::Default, "DOC") << "Commit ActivateOperation state: " << fis.waitReceived(InvocationState::Fin, 2000);

			return true;
		}
		else {
			Util::DebugOut(Util::DebugOut::Error, "DOC") << "Cannot invoke ActivateOperation as there is no associated device to be controlled!" << std::endl;
			return false;
		}
	}

	bool startMeasurement()
	{
		assert(nullptr != m_control_consumer);
		if ( nullptr != m_control_consumer ) {
			FutureInvocationState fis;
			m_control_consumer->activate(HANDLE_CONTROL_COMMAND_START, fis);
			Util::DebugOut(Util::DebugOut::Default, "DOC") << "Commit ActivateOperation for triggering measurement start - state: " << fis.waitReceived(InvocationState::Fin, 2000) << std::endl;

			return true;
		}
		else {
			Util::DebugOut(Util::DebugOut::Error, "DOC") << "Cannot invoke ActivateOperation for triggering measurement start as there is no associated device to be controlled!" << std::endl;
			return false;
		}
	}

	void registerEventHandler(SDCConsumerOperationInvokedHandler * p_handler) {
		std::cout << "register: " << m_control_consumer->registerStateEventHandler(p_handler) << std::endl;
	}


	void registerEventHandlerControlled(SDCConsumerOperationInvokedHandler * p_handler) {
		m_controlled_consumer->registerStateEventHandler(p_handler);
	}


	void setControlledConsumer(std::unique_ptr<SDCLib::Data::SDC::SDCConsumer> p_controlledConsumer) {
		m_controlled_consumer = std::move(p_controlledConsumer);
	}

	std::string getEprOfControlDev() {
		return m_control_consumer->getEndpointReference();
	}
};


using ControlConsumerWrapper_shared_ptr = std::shared_ptr<ControlConsumerWrapper>;



/**
 * This class is responsible for handle events coming from the control, e.g. foot switch
 */
class ControlEventHandler : public SDCConsumerMDStateHandler<NumericMetricState>
{
private:
    ControlConsumerWrapper_shared_ptr m_wrapper = nullptr;

public:
    ControlEventHandler(const std::string & handle, ControlConsumerWrapper_shared_ptr p_wrapper)
	: SDCConsumerMDStateHandler(handle)
	, m_wrapper(p_wrapper)
	{
    	assert(nullptr != m_wrapper);
	}

    /**
     * in this very simple case, we trigger the controlled device every time the indicator metric changes
     * TODO If the indicator metric really indicates pressed and released, this is NOT sufficient!
     */
    void onStateChanged(const NumericMetricState & state) override
    {
    	DebugOut(DebugOut::Default, "DOC") << "Consumer: Received state change from Control with EPR " << m_wrapper->getEprOfControlDev() <<  " of descriptor: " << this->getDescriptorHandle() << ". New value is: " << state.getMetricValue().getValue() << std::endl;

        if (m_wrapper != nullptr) {
        		m_wrapper->activate();
        }
        else {
        	std::cerr << "Cannot trigger activate! Wrapper == NULL :-(" << std::endl;
        }

    }

    void onOperationInvoked(const OperationInvocationContext & oic, InvocationState is) override
    {
        DebugOut(DebugOut::Default, "DOC") << "Consumer: Received operation invoked (numeric metric) (ID, STATE) of " << this->getDescriptorHandle() << ": " << oic.transactionId << ", " << Data::SDC::EnumToString::convert(is) << std::endl;
    }
};

class ControlledDummyEventHandler : public SDCConsumerMDStateHandler<NumericMetricState>
{


public:
    ControlledDummyEventHandler(const std::string & handle)
	: SDCConsumerMDStateHandler(handle)
	{
	}

    /**
     * in this very simple case, we trigger the controlled device every time the indicator metric changes
     * TODO If the indicator metric really indicates pressed and released, this is NOT sufficient!
     */
    void onStateChanged(const NumericMetricState & state) override
    {
    	DebugOut(DebugOut::Default, "DOC") << "Consumer: Received state change of descriptor: " << this->getDescriptorHandle() << ". New value is: " << state.getMetricValue().getValue() << std::endl;
    }

    void onOperationInvoked(const OperationInvocationContext & oic, InvocationState is) override
    {
        DebugOut(DebugOut::Default, "DOC") << "Consumer: Received operation invoked (numeric metric) (ID, STATE) of " << this->getDescriptorHandle() << ": " << oic.transactionId << ", " << Data::SDC::EnumToString::convert(is) << std::endl;
    }
};



void waitForUserInput() {
	std::string temp;
	std::cout << "Press key to proceed." << std::endl;
	std::cin >> temp;
}


int main()
{
	EprHandling *eprHandling = new EprHandling();

	int controlHits = 0;
	int controlledHits = 0;
	int registeredControl = 0;
	int registeredController = 0;

	Util::DebugOut(Util::DebugOut::Default, "DOC") << "Startup";
    SDCLibrary::getInstance().startup(OSELib::LogLevel::Error);
    DebugOut::DEBUG_LEVEL = DebugOut::Silent; //configure log level

    class MyConnectionLostHandler : public Data::SDC::SDCConsumerConnectionLostHandler {
    public:
    	MyConnectionLostHandler(Data::SDC::SDCConsumer & consumer) : consumer(consumer) {
    	}
    	void onConnectionLost() override {
    		std::cerr << "Connection lost, disconnecting... ";
    		consumer.disconnect();
    		std::cerr << "disconnected." << std::endl;
    	}
    private:
    	Data::SDC::SDCConsumer & consumer;
    };

    // Create a new SDCInstance (no flag will auto init)
    auto t_SDCInstance = std::make_shared<SDCInstance>(true);
    // Some restriction
    t_SDCInstance->setIP6enabled(false);
    t_SDCInstance->setIP4enabled(true);
    // Bind it to interface that matches the internal criteria (usually the first enumerated)
    if(!t_SDCInstance->bindToDefaultNetworkInterface()) {
        std::cerr << "Failed to bind to default network interface! Exit..." << std::endl;
        return -1;
    }

	// Discovery
	OSELib::SDC::ServiceManager t_serviceManager(t_SDCInstance);


	try {

		/**
		 * map collecting the wrappers of the control device consumers
		 */
		std::map< std::string, ControlConsumerWrapper_shared_ptr> tm_controlWrapper;

		/**
		 * map collecting the consumers for the controlled devices
		 */
		std::map< std::string, std::unique_ptr<Data::SDC::SDCConsumer> > tm_controlledMap;

		/**
		 * vector collecting the event handlers
		 */
		std::vector<std::shared_ptr<ControlEventHandler>> tl_eh;

		/**
		 * vector collecting the event handlers
		 */
		std::vector<std::shared_ptr<ControlledDummyEventHandler>> tl_dummy_eh;


		auto tl_consumers = t_serviceManager.discover();
		for (auto & t_consumer : tl_consumers) {
			std::string foundEpr = t_consumer->getEndpointReference();
		    std::cout << "Found: " << foundEpr << std::endl;



		    if ( eprHandling->isControlEpr(t_consumer->getEndpointReference()) ) {
		    	std::cout << "hit control" << std::endl;
		    	controlHits++;

		    	auto t_control_wrapper = std::make_shared<ControlConsumerWrapper>(std::move(t_consumer));

		    	foundEpr.pop_back();
		    	tm_controlWrapper[foundEpr] = t_control_wrapper;
		    }
		    else if ( eprHandling->isControlledEpr(t_consumer->getEndpointReference()) ) {
		    	std::cout << "hit controlled" << std::endl;
		    	controlledHits++;

		    	foundEpr.pop_back();
		    	tm_controlledMap[foundEpr] = std::move(t_consumer);
		    }
		}


		// let's have a look on what we found
		for (auto & t_control_wrapper_entry : tm_controlWrapper)
		{

			std::string eprKey = t_control_wrapper_entry.first;

			// look whether there is a corresponding controlled device
			auto t_iteratror_controller = tm_controlledMap.find(eprKey);
			if ( t_iteratror_controller == tm_controlledMap.end() ) {
				std::cerr << "There is no controlled device for the controller with EPR: " << eprKey << "*" << std::endl;
			}
			else {
				// now we have the pair of control device and controlled device
				// thus, we can connect them and can build up the event handlers

				// get the control wrapper
				auto t_control_wrapper = t_control_wrapper_entry.second;

				// connect the controlled device to the control
				t_control_wrapper->setControlledConsumer(std::move(t_iteratror_controller->second));

				// state handler for the control (e.g. foot switch)
				auto eh_controlCounterMetric = std::make_shared<ControlEventHandler>(HANDLE_CONTROL_INDICATOR_METRIC, t_control_wrapper);
				tl_eh.push_back(eh_controlCounterMetric);

				t_control_wrapper->registerEventHandler(eh_controlCounterMetric.get());
				registeredControl++;

				// dummy state handler for the controlled device
				auto eh_controlledCounterMetric = std::make_shared<ControlledDummyEventHandler>("h_counter");
				tl_dummy_eh.push_back(eh_controlledCounterMetric);

				t_control_wrapper->registerEventHandler(eh_controlledCounterMetric.get());
				registeredController++;

			}
		}

		std::cout << std::endl << "################################" << std::endl;
		std::cout << "Hits for control providers: " << controlHits << std::endl;
		std::cout << "Hits for controlled providers: " << controlledHits << std::endl;
		std::cout << "Registered for control providers: " << registeredControl << std::endl;
		std::cout << "Registered for controlled providers: " << registeredController << std::endl;
		std::cout << "################################" << std::endl;


		waitStartup();


		for (auto & t_control_wrapper_entry : tm_controlWrapper)
		{
			std::cout << "Trigger measurement start for Control Device ( "
					"" << t_control_wrapper_entry.second->getEprOfControlDev() <<  "): "
							"" << t_control_wrapper_entry.second->startMeasurement() << "." <<  std::endl;
		}

		waitForUserInput();


	} catch (std::exception & e){
		Util::DebugOut(Util::DebugOut::Default, "DOC") << "Exception: " << e.what() << std::endl;
	}
    SDCLibrary::getInstance().shutdown();
    Util::DebugOut(Util::DebugOut::Default, "DOC") << "Shutdown." << std::endl;
}
