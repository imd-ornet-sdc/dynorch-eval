/*
 * EprHandling.h
 *
 *  Created on: 07.11.2019
 *      Author: martin
 */

#ifndef SRC_DOC_EPRHANDLING_H_
#define SRC_DOC_EPRHANDLING_H_

#include <vector>
#include <string>
#include <algorithm>

/**
 * By convention for this test tool: the pair of devices to be associated
 * have almost the same EPR. The control device EPR ends with "0"; the
 * controlled device EPR ends with "1"
 */
class EprHandling {
public:
	EprHandling();
	virtual ~EprHandling();

	const std::vector<std::string> EPR_LIST_CONTROL_DEV = {
			"urn:uuid:2299249a-0167-5dde-9acf-000000001000",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000150",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000160",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000170",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000180",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000190",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000210",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000220",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000230",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000240",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000250"
	};

	const std::vector<std::string> EPR_LIST_CONTROLLED_DEV = {
			"urn:uuid:2299249a-0167-5dde-9acf-000000001001",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000151",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000161",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000171",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000181",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000191",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000211",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000221",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000231",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000241",
			"urn:uuid:2299249a-0167-5dde-9acf-000000000251"
	};

	/**
	 * @see https://thispointer.com/c-how-to-find-an-element-in-vector-and-get-its-index/
	 * This method is taken from the given web source
	 *
	 * Generic function to find an element in vector and also its position.
	 * It returns a pair of bool & int i.e.
	 * bool : Represents if element is present in vector or not.
	 * int : Represents the index of element in vector if its found else -1
	 *
	 * usage example:
	 * std::pair<bool, int> result = findInVector<int>(vecOfNums, 45);
	 * if (result.first)
	 * 		std::cout << "Element Found at index : " << result.second <<std::endl;
	 * else
	 * 		std::cout << "Element Not Found" << std::endl;
	 */
	template < typename T>
	std::pair<bool, int > findInVector(const std::vector<T>  & vecOfElements, const T  & element)
	{
		std::pair<bool, int > result;

		// Find given element in vector
		auto it = std::find(vecOfElements.begin(), vecOfElements.end(), element);

		if (it != vecOfElements.end())
		{
			result.second = distance(vecOfElements.begin(), it);
			result.first = true;
		}
		else
		{
			result.first = false;
			result.second = -1;
		}

		return result;
	}

	/**
	 * method to check whether the given -epr is in the list of control device EPRs
	 */
	bool isControlEpr(std::string p_epr) {
		std::pair<bool, int> result = findInVector<std::string>(EPR_LIST_CONTROL_DEV, p_epr);
		return result.first;
	}

	/**
	 * method to check whether the given -epr is in the list of control device EPRs
	 */
	bool isControlledEpr(std::string p_epr) {
		std::pair<bool, int> result = findInVector<std::string>(EPR_LIST_CONTROLLED_DEV, p_epr);
		return result.first;
	}
};

#endif /* SRC_DOC_EPRHANDLING_H_ */
