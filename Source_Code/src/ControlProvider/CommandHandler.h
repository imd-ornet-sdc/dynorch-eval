/*
 * CommandHandler.h
 *
 *  Created on: 11.11.2019
 *      Author: martin
 */

#ifndef SRC_CONTROLPROVIDER_COMMANDHANDLER_H_
#define SRC_CONTROLPROVIDER_COMMANDHANDLER_H_



#include <iostream>

#include "SDCLib/Data/SDC/SDCProvider.h"

#include "SDCLib/SDCLibrary.h"
#include "SDCLib/SDCInstance.h"

#include "SDCLib/Data/SDC/SDCProviderComponentStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderMDStateHandler.h"
#include "SDCLib/Data/SDC/MDIB/ChannelDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/CodedValue.h"
#include "SDCLib/Data/SDC/MDIB/SimpleTypesMapping.h"
#include "SDCLib/Data/SDC/MDIB/MdsDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/LocalizedText.h"
#include "SDCLib/Data/SDC/MDIB/MdDescription.h"
#include "SDCLib/Data/SDC/MDIB/MetricQuality.h"
#include "SDCLib/Data/SDC/MDIB/Range.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricState.h"
#include "SDCLib/Data/SDC/MDIB/SampleArrayValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricState.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricState.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/SystemContextDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/MetaData.h"
#include "SDCLib/Dev/DeviceCharacteristics.h"
#include "SDCLib/Data/SDC/MDIB/VmdDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/custom/OperationInvocationContext.h"
#include "SDCLib/Data/SDC/SDCProviderActivateOperationHandler.h"
#include "SDCLib/Data/SDC/MDIB/ActivateOperationDescriptor.h"

#include "SDCLib/Data/SDC/SDCConsumer.h"
#include "SDCLib/Data/SDC/SDCConsumerConnectionLostHandler.h"
#include "SDCLib/Data/SDC/SDCConsumerMDStateHandler.h"
#include "OSELib/SDC/ServiceManager.h"

#include "SDCLib/Util/DebugOut.h"
#include "SDCLib/Util/Task.h"

#include "../util/LatencyMeasurement.h"

#include <MyConstants.h>

using namespace SDCLib;
using namespace SDCLib::Util;
using namespace SDCLib::Data::SDC;

#pragma once
class SdcControlProvider;

// the control provider provides an additional activate operation to trigger the evaluation process
class CommandHandler : public SDCProviderActivateOperationHandler {
private:
	SdcControlProvider *m_provider;
public:

	virtual ~CommandHandler(){};

	CommandHandler(std::string descriptorHandle, SdcControlProvider *p_provider)
	:
		SDCProviderActivateOperationHandler(descriptorHandle),
		m_provider(p_provider)
	{
    }

	InvocationState onActivateRequest(const OperationInvocationContext&) override;
};
#endif /* SRC_CONTROLPROVIDER_COMMANDHANDLER_H_ */
