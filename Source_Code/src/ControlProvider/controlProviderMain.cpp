#include <iostream>

#include "SDCLib/Data/SDC/SDCProvider.h"

#include "SDCLib/SDCLibrary.h"
#include "SDCLib/SDCInstance.h"

#include "SDCLib/Data/SDC/SDCProviderComponentStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderMDStateHandler.h"
#include "SDCLib/Data/SDC/MDIB/ChannelDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/CodedValue.h"
#include "SDCLib/Data/SDC/MDIB/SimpleTypesMapping.h"
#include "SDCLib/Data/SDC/MDIB/MdsDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/LocalizedText.h"
#include "SDCLib/Data/SDC/MDIB/MdDescription.h"
#include "SDCLib/Data/SDC/MDIB/MetricQuality.h"
#include "SDCLib/Data/SDC/MDIB/Range.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricState.h"
#include "SDCLib/Data/SDC/MDIB/SampleArrayValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricState.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricState.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/SystemContextDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/MetaData.h"
#include "SDCLib/Dev/DeviceCharacteristics.h"
#include "SDCLib/Data/SDC/MDIB/VmdDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/custom/OperationInvocationContext.h"
#include "SDCLib/Data/SDC/SDCProviderActivateOperationHandler.h"
#include "SDCLib/Data/SDC/MDIB/ActivateOperationDescriptor.h"

#include "SDCLib/Data/SDC/SDCConsumer.h"
#include "SDCLib/Data/SDC/SDCConsumerConnectionLostHandler.h"
#include "SDCLib/Data/SDC/SDCConsumerMDStateHandler.h"
#include "OSELib/SDC/ServiceManager.h"

#include "SDCLib/Util/DebugOut.h"
#include "SDCLib/Util/Task.h"

#include "../util/LatencyMeasurement.h"

#include <CommandHandler.h>
#include <SdcControlProvider.h>
#include <NumericProviderStateHandlerGet.h>
#include <MyConstants.h>
#include "../sharedClasses/ControlledDeviceCounterEventHandler.h"

using namespace SDCLib;
using namespace SDCLib::Util;
using namespace SDCLib::Data::SDC;



//const std::string deviceEPR("urn:uuid:4242-control");
std::string deviceEPR("urn:uuid:2299249a-0167-5dde-9acf-000000000000");

//@see: https://www.modernescpp.com/index.php/c-core-guidelines-be-aware-of-the-traps-of-condition-variables
std::condition_variable m_receivedEventCondVar; // trigger to signal that the event was received
std::mutex m_receivedEventMutex; // corresponding mutex for the conditional variable
std::atomic<bool> m_receivedEventBoolean{false}; // additional flag to signal the received data


class SdcControlProvider; //forward declaration


int main(int argc, char* argv[])
{

	LatencyMeasurement::getInstance().clearAllMeasurements();

	auto now = std::chrono::high_resolution_clock::now();
	auto nanos = std::chrono::duration_cast<std::chrono::nanoseconds>(now.time_since_epoch()).count();

	if(argc != 4){
		std::cerr << "wrong number of arguments: " << argc << std::endl;
		std::cerr << "usage: ./controlProvider < EPR > < #ofRuns > < intervalBetweenTriggersInMS >" << std::endl;
		return -1;
	}

	deviceEPR = argv[1];
	int numberOfRuns = atoi(argv[2]);
	int interval = atoi(argv[3]);

	// produce seed using the last digits of the EPR -> attention: this requires numeric last digits!
	std::string tmpSubEpr = deviceEPR.substr( deviceEPR.length() - 5 );
	int seed = atoi(tmpSubEpr.c_str());


	std::list<long long> startTimestamps;
	std::list<long long> stopTimestamps;


	// Startup
	DebugOut(DebugOut::Default, "Control Provider") << "Startup" << std::endl;
	SDCLibrary::getInstance().startup(OSELib::LogLevel::Error);
	DebugOut::DEBUG_LEVEL = DebugOut::Silent; //configure log level

	// Create a new SDCInstance (no flag will auto init)
	auto t_SDCInstance = std::make_shared<SDCInstance>(true);
	
	// Some restriction
	t_SDCInstance->setIP6enabled(false);
	t_SDCInstance->setIP4enabled(true);
	
	// Bind it to interface that matches the internal criteria (usually the first enumerated)
	if(!t_SDCInstance->bindToDefaultNetworkInterface()) {
		DebugOut(DebugOut::Default, "Control Provider") << "Failed to bind to default network interface! Exit..." << std::endl;
		return -1;
	}

	SdcControlProvider provider(t_SDCInstance, deviceEPR, numberOfRuns, interval, seed, &startTimestamps,
			m_receivedEventCondVar,
			m_receivedEventMutex,
			m_receivedEventBoolean);
	provider.startup();


	// consumer part -> this is only for measurement purposes
	// Create a new SDCInstance (no flag will auto init)
	auto t_SDCInstanceConsumer = std::make_shared<SDCInstance>(true);
	// Some restriction
	t_SDCInstanceConsumer->setIP6enabled(false);
	t_SDCInstanceConsumer->setIP4enabled(true);
	// Bind it to interface that matches the internal criteria (usually the first enumerated)
	if(!t_SDCInstanceConsumer->bindToDefaultNetworkInterface()) {
		std::cout << "Failed to bind to default network interface! Exit..." << std::endl;
		return -1;
	}

	OSELib::SDC::ServiceManager t_serviceManager(t_SDCInstanceConsumer);

	// build the EPR of the corresponding controlled device
	std::string controlledDeviceEpr = deviceEPR;
	controlledDeviceEpr.pop_back(); //delete last character
	controlledDeviceEpr.append("1"); // add 1 at the end -> this is the corresponding EPR

	auto t_consumer(t_serviceManager.discoverEndpointReference(controlledDeviceEpr));

	if (t_consumer != nullptr) {
		std::cout << "Found controlled device with EPR: " << t_consumer->getEndpointReference() << std::endl;

		auto teh_controlledDevCounterMetric =
				std::make_shared<ControlledDeviceCounterEventHandler>(HANDLE_CONTROLLED_DEV_COUNTER_METRIC, &stopTimestamps,
				m_receivedEventCondVar,
				m_receivedEventBoolean);
		t_consumer->registerStateEventHandler(teh_controlledDevCounterMetric.get());

		std::string temp;

		std::cout << "Press key to exit program." << std::endl;
		std::cin >> temp;
	}
	else {
		std::cerr << "Did NOT find a corresponding controlled device with EPR " << controlledDeviceEpr << std::endl;
	}


	for (auto &elem : startTimestamps) {
		std::cout << elem << std::endl;
	}

	LatencyMeasurement::getInstance().setT0List(startTimestamps);
	LatencyMeasurement::getInstance().setT1List(stopTimestamps);
	LatencyMeasurement::getInstance().writeEveryMeasurementToFiles(true);

	// Shutdown
	DebugOut(DebugOut::Default, "Control Provider") << "Shutdown." << std::endl;
	provider.shutdown();
	SDCLibrary::getInstance().shutdown();

    return 0;
}
