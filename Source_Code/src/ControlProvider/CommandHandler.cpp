/*
 * CommandHandler.cpp
 *
 *  Created on: 13.11.2019
 *      Author: martin
 */

#include <CommandHandler.h>
#include <SdcControlProvider.h>

InvocationState CommandHandler::onActivateRequest(const OperationInvocationContext&) {
		DebugOut(DebugOut::Default, "SimpleSDC") << "Provider: Received command! Start..." << std::endl;
		m_provider->start(); // start the evaluation process
		return InvocationState::Fin;
	}
