/*
 * SdcControlProvider.cpp
 *
 *  Created on: 13.11.2019
 *      Author: martin
 */

#include <SdcControlProvider.h>
/*
SdcControlProvider2::SdcControlProvider2(SDCInstance_shared_ptr p_SDCInstance, std::string p_deviceEpr, int p_numberOfRuns, std::list<long long> *p_startTimestamps) :
		m_deviceEpr(p_deviceEpr),
		sdcProvider(p_SDCInstance),
		numericProviderStateHandlerGet(HANDLE_CONTROL_INDICATOR_METRIC),
		getMetricDescriptor(HANDLE_CONTROL_INDICATOR_METRIC,
						CodedValue("MDCX_EXAMPLE_GET"),
						MetricCategory::Set,
						MetricAvailability::Cont,
						1),
		cmdHandler(HANDLE_COMMAND, this),
		numberOfRuns(p_numberOfRuns),
		m_startTimestamps(p_startTimestamps)
	{

		// set DPWS metadata, e.g. for the displayed friendly name
		Dev::DeviceCharacteristics devChar;
		devChar.addFriendlyName("en", "SDCLib C SdcControlProvider");
		devChar.setManufacturer("SurgiTAIX AG");
		devChar.addModelName("en", "SdcControlProvider");
		sdcProvider.setDeviceCharacteristics(devChar);
		sdcProvider.setEndpointReference(m_deviceEpr);

		// build up the MdDescription
		// Channel
		ChannelDescriptor channelDescr("h_channel");
		channelDescr
			.setSafetyClassification(SafetyClassification::MedC)
			.addMetric(getMetricDescriptor);

		// VMD
		VmdDescriptor vmdDescr("h_vmd");
		vmdDescr.addChannel(channelDescr);

        // MDS
        MdsDescriptor mdsDescr("h_mds");
        mdsDescr
        	.setType(CodedValue("67170304")) //1024::61440 = 1024::0xF000
        	.setMetaData(
                MetaData().addManufacturer(LocalizedText().setRef(SDCLib::Config::STR_SURGITAIX))
                .setModelNumber("1")
                .addModelName(LocalizedText().setRef("IMD University of Rostock"))
                .addSerialNumber(SDCLib::Config::CURRENT_C_YEAR))
        	.addVmd(vmdDescr);

        ActivateOperationDescriptor aod(HANDLE_COMMAND, HANDLE_CONTROL_INDICATOR_METRIC);
        aod.setRetriggerable(true);

		sdcProvider.addActivateOperationForDescriptor(aod, mdsDescr);


        // create and add description
		MdDescription mdDescription;
		mdDescription.addMdsDescriptor(mdsDescr);

		sdcProvider.setMdDescription(mdDescription);

		// Add handler
		sdcProvider.addMdStateHandler(&numericProviderStateHandlerGet);
		sdcProvider.addMdStateHandler(&cmdHandler);
	}

*/
