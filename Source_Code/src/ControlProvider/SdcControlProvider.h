/*
 * SdcControlProvider.h
 *
 *  Created on: 11.11.2019
 *      Author: martin
 */

#ifndef SRC_CONTROLPROVIDER_SDCCONTROLPROVIDER_H_
#define SRC_CONTROLPROVIDER_SDCCONTROLPROVIDER_H_


#include <CommandHandler.h>
#include <MyConstants.h>
#include <NumericProviderStateHandlerGet.h>
#include <random>

using namespace SDCLib;
using namespace SDCLib::Util;
using namespace SDCLib::Data::SDC;

#pragma once
class CommandHandler;

class SdcControlProvider : public Util::Task {
private:
	std::string m_deviceEpr;

	SDCProvider sdcProvider;
	NumericProviderStateHandlerGet numericProviderStateHandlerGet;
	NumericMetricDescriptor getMetricDescriptor;
	CommandHandler cmdHandler;
	int numberOfRuns;
	int interval;
	int seed;
	std::list<long long> *m_startTimestamps;

	//@see: https://www.modernescpp.com/index.php/c-core-guidelines-be-aware-of-the-traps-of-condition-variables
	std::condition_variable& m_receivedEventCondVar; // trigger to signal that the event was received
	std::mutex& m_receivedEventMutex; // corresponding mutex for the conditional variable
	std::atomic<bool>& m_receivedEventBoolean; // additional flag to signal the received data

public:
	SdcControlProvider(SDCInstance_shared_ptr p_SDCInstance, std::string p_deviceEpr, int p_numberOfRuns, int p_interval, int p_seed, std::list<long long> *p_startTimestamps, std::condition_variable& p_receivedEventCondVar,
			std::mutex& p_receivedEventMutex,
			std::atomic<bool>& p_receivedEventBoolean) :
		m_deviceEpr(p_deviceEpr),
		sdcProvider(p_SDCInstance),
		numericProviderStateHandlerGet(HANDLE_CONTROL_INDICATOR_METRIC),
		getMetricDescriptor(HANDLE_CONTROL_INDICATOR_METRIC,
						CodedValue("MDCX_EXAMPLE_GET"),
						MetricCategory::Set,
						MetricAvailability::Cont,
						1),
		cmdHandler(HANDLE_COMMAND, this),
		numberOfRuns(p_numberOfRuns),
		interval(p_interval),
		seed(p_seed),
		m_startTimestamps(p_startTimestamps),
		m_receivedEventCondVar(p_receivedEventCondVar),
		m_receivedEventMutex(p_receivedEventMutex),
		m_receivedEventBoolean(p_receivedEventBoolean)
	{

		// set DPWS metadata, e.g. for the displayed friendly name
		Dev::DeviceCharacteristics devChar;
		devChar.addFriendlyName("en", "SDCLib C SdcControlProvider");
		devChar.setManufacturer("IMD Uni Rostock");
		devChar.addModelName("en", "SdcControlProvider");
		sdcProvider.setDeviceCharacteristics(devChar);
		sdcProvider.setEndpointReference(m_deviceEpr);

		// build up the MdDescription
		// Channel
		ChannelDescriptor channelDescr("h_channel");
		channelDescr
			.setSafetyClassification(SafetyClassification::MedC)
			.addMetric(getMetricDescriptor);

		// VMD
		VmdDescriptor vmdDescr("h_vmd");
		vmdDescr.addChannel(channelDescr);

        // MDS
        MdsDescriptor mdsDescr("h_mds");
        mdsDescr
        	.setType(CodedValue("67170304")) //1024::61440 = 1024::0xF000
        	.setMetaData(
                MetaData().addManufacturer(LocalizedText().setRef(SDCLib::Config::STR_SURGITAIX))
                .setModelNumber("1")
                .addModelName(LocalizedText().setRef("IMD University of Rostock"))
                .addSerialNumber(SDCLib::Config::CURRENT_C_YEAR))
        	.addVmd(vmdDescr);

        ActivateOperationDescriptor aod(HANDLE_COMMAND, HANDLE_CONTROL_INDICATOR_METRIC);
        aod.setRetriggerable(true);

		sdcProvider.addActivateOperationForDescriptor(aod, mdsDescr);


        // create and add description
		MdDescription mdDescription;
		mdDescription.addMdsDescriptor(mdsDescr);

		sdcProvider.setMdDescription(mdDescription);

		// Add handler
		sdcProvider.addMdStateHandler(&numericProviderStateHandlerGet);
		sdcProvider.addMdStateHandler(&cmdHandler);
	}

    void startup() {
    	sdcProvider.startup();
    }

    void shutdown() {
    	sdcProvider.shutdown();
    }

    // runImpl() gets called when starting the provider thread by the inherited function start()
    virtual void runImpl() override {


		std::size_t index(0);
		int runNum = 0;

		std::mt19937 eng(seed); // seed the generator -> pseudo seed for semi-reproducibility
		std::uniform_int_distribution<> distr(0, 5); // define the range

		while (!isInterrupted()) {

			// take the timestamp
			auto nowTS = std::chrono::steady_clock::now();
			long long nanosTS = std::chrono::duration_cast<std::chrono::nanoseconds>(nowTS.time_since_epoch()).count();
			m_startTimestamps->push_back(nanosTS);

			// Update the NumericMetricState's value using the state handler's method
			numericProviderStateHandlerGet.setNumericValue(index);
			DebugOut(DebugOut::Default, "ExampleProvider") << "NumericMetric: value changed to " << index << std::endl;

			// Block here
			std::unique_lock<std::mutex> uLock(m_receivedEventMutex);
			m_receivedEventCondVar.wait(uLock, [&]{return m_receivedEventBoolean.load();});
			m_receivedEventBoolean.store(false); //reset flag


			// sleep a certain time before triggering the next value change
			std::this_thread::sleep_for(std::chrono::milliseconds(interval + distr(eng)));
			index++;

			if ( ++runNum >= numberOfRuns ) {
				std::cout << "Finished after " << runNum << " runs :-D" << std::endl;
				this->interrupt();
			}
		}

    }
};

#endif /* SRC_CONTROLPROVIDER_SDCCONTROLPROVIDER_H_ */
