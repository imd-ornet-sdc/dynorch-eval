#include <iostream>

#include "SDCLib/Data/SDC/SDCProvider.h"

#include "SDCLib/SDCLibrary.h"
#include "SDCLib/SDCInstance.h"

#include "SDCLib/Data/SDC/SDCProviderComponentStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderStateHandler.h"
#include "SDCLib/Data/SDC/SDCProviderMDStateHandler.h"
#include "SDCLib/Data/SDC/MDIB/ChannelDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/CodedValue.h"
#include "SDCLib/Data/SDC/MDIB/SimpleTypesMapping.h"
#include "SDCLib/Data/SDC/MDIB/MdsDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/LocalizedText.h"
#include "SDCLib/Data/SDC/MDIB/MdDescription.h"
#include "SDCLib/Data/SDC/MDIB/MetricQuality.h"
#include "SDCLib/Data/SDC/MDIB/Range.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/RealTimeSampleArrayMetricState.h"
#include "SDCLib/Data/SDC/MDIB/SampleArrayValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricState.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/NumericMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricState.h"
#include "SDCLib/Data/SDC/MDIB/StringMetricValue.h"
#include "SDCLib/Data/SDC/MDIB/SystemContextDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/MetaData.h"
#include "SDCLib/Dev/DeviceCharacteristics.h"
#include "SDCLib/Data/SDC/MDIB/VmdDescriptor.h"
#include "SDCLib/Data/SDC/MDIB/custom/OperationInvocationContext.h"
#include "SDCLib/Data/SDC/SDCProviderActivateOperationHandler.h"
#include "SDCLib/Data/SDC/MDIB/ActivateOperationDescriptor.h"

#include "SDCLib/Util/DebugOut.h"
#include "SDCLib/Util/Task.h"

using namespace SDCLib;
using namespace SDCLib::Util;
using namespace SDCLib::Data::SDC;

const std::string CLASS_NAME("SdcControlledProvider");
const std::string HANDLE_COUNTER_METRIC("h_counter");
const std::string HANDLE_COMMAND("h_act");

//std::string deviceEPR("urn:uuid:2299249a-0167-5dde-9acf-000000000001");
std::string deviceEPR("urn:uuid:temp-epr");

class NumericProviderStateHandlerGet : public SDCProviderMDStateHandler<NumericMetricState> {
private:
	double currentValue = 0;
public:

	NumericProviderStateHandlerGet(std::string descriptorHandle) : SDCProviderMDStateHandler(descriptorHandle) {
	}


	// Helper method
	NumericMetricState createState(double value) {
		NumericMetricState result(HANDLE_COUNTER_METRIC);
		result
			.setMetricValue(NumericMetricValue(MetricQuality(MeasurementValidity::Vld)).setValue(value))
			.setActivationState(ComponentActivation::On);
		return result;
	}

	// define how to react on a request for a state change. This handler should not be set, thus always return Fail.
	InvocationState onStateChangeRequest(const NumericMetricState& nms, const OperationInvocationContext & oic) override {
		// extract information from the incoming operation
		DebugOut(DebugOut::Default, CLASS_NAME) << "Operation invoked. Handle: " << oic.operationHandle << std::endl;
		return InvocationState::Fail;
	}


	NumericMetricState getInitialState() override {
		NumericMetricState nms = createState(0);
		return nms;
	}

	void setNumericValue(double value) {
		NumericMetricState nms = createState(value);
		updateState(nms);
		DebugOut(DebugOut::Default, CLASS_NAME) << "New Value: " << nms.getMetricValue().getValue() << std::endl;
	}

	void incrementValue() {
		NumericMetricState nms = createState(++currentValue);
		updateState(nms);
		DebugOut(DebugOut::Default, CLASS_NAME) << "New Value: " << nms.getMetricValue().getValue() << " (" << this->descriptorHandle << ")"<< std::endl;

	}
};

// use this state handler for remote function calls
class CommandHandler : public SDCProviderActivateOperationHandler {
private:
	NumericProviderStateHandlerGet *numericProviderStateHandlerGet;
public:

	CommandHandler(std::string descriptorHandle, NumericProviderStateHandlerGet *_numericProviderStateHandlerGet) : SDCProviderActivateOperationHandler(descriptorHandle) {
		this->numericProviderStateHandlerGet = _numericProviderStateHandlerGet;
    }

	InvocationState onActivateRequest(const OperationInvocationContext&) override {
		DebugOut(DebugOut::Default, CLASS_NAME) << "Provider: Received command!" << std::endl;
		numericProviderStateHandlerGet->incrementValue();
		return InvocationState::Fin;
	}
};

class SdcControlProvider {
	
private: 
	SDCProvider sdcProvider;
	NumericProviderStateHandlerGet numericProviderStateHandlerGet;
	NumericMetricDescriptor getMetricDescriptor;
	CommandHandler cmdHandler;

	int currentCounterValue = 0;

public: 
	
	SdcControlProvider(SDCInstance_shared_ptr p_SDCInstance) :
		sdcProvider(p_SDCInstance),
		numericProviderStateHandlerGet(HANDLE_COUNTER_METRIC),
		getMetricDescriptor(HANDLE_COUNTER_METRIC,
						CodedValue("MDCX_EXAMPLE_GET"),
						MetricCategory::Set,
						MetricAvailability::Cont,
						1),
		cmdHandler(HANDLE_COMMAND, &numericProviderStateHandlerGet)
	{

		// set DPWS metadata, e.g. for the displayed friendly name
		Dev::DeviceCharacteristics devChar;
		devChar.addFriendlyName("en", "SDCLib C SdcControlProvider");
		devChar.setManufacturer("IMD Uni Rostock");
		devChar.addModelName("en", "SdcControlProvider");
		sdcProvider.setDeviceCharacteristics(devChar);

		sdcProvider.setEndpointReference(deviceEPR);

		// build up the MdDescription
		// Channel
		ChannelDescriptor channelDescr("h_channel");
		channelDescr
			.setSafetyClassification(SafetyClassification::MedC)
			.addMetric(getMetricDescriptor);

		// VMD
		VmdDescriptor vmdDescr("h_vmd");
		vmdDescr.addChannel(channelDescr);

        // MDS
        MdsDescriptor mdsDescr("h_mds");
        mdsDescr
        	.setType(CodedValue("67170304")) //1024::61440 = 1024::0xF000
        	.setMetaData(
                MetaData().addManufacturer(LocalizedText().setRef(SDCLib::Config::STR_SURGITAIX))
                .setModelNumber("1")
                .addModelName(LocalizedText().setRef("IMD University of Rostock"))
                .addSerialNumber(SDCLib::Config::CURRENT_C_YEAR))
        	.addVmd(vmdDescr);

        ActivateOperationDescriptor aod(HANDLE_COMMAND, HANDLE_COUNTER_METRIC);
        aod.setRetriggerable(true);

		sdcProvider.addActivateOperationForDescriptor(aod, mdsDescr);


        // create and add description
		MdDescription mdDescription;
		mdDescription.addMdsDescriptor(mdsDescr);

		sdcProvider.setMdDescription(mdDescription);

		// Add handler
		sdcProvider.addMdStateHandler(&numericProviderStateHandlerGet);
		sdcProvider.addMdStateHandler(&cmdHandler);
	}
	
    void startup() {
    	sdcProvider.startup();
    	DebugOut(DebugOut::Default, CLASS_NAME) << "EPR: " << sdcProvider.getEndpointReference() << std::endl;
    }

    void shutdown() {
    	sdcProvider.shutdown();
    }

    void incrementCounterMetric() {
    	numericProviderStateHandlerGet.setNumericValue(++currentCounterValue);
    }

};


int main(int argc, char* argv[])
{
	if(argc != 2){
		std::cerr << "wrong number of arguments: " << argc << std::endl;
		std::cerr << "usage: ./controlledProvider EPR" << std::endl;
		return -1;
	}

	deviceEPR = argv[1];


	// Startup
	DebugOut(DebugOut::Default, CLASS_NAME) << "Startup" << std::endl;
	SDCLibrary::getInstance().startup(OSELib::LogLevel::Error);
	DebugOut::DEBUG_LEVEL = DebugOut::Silent; //configure log level

	// Create a new SDCInstance (no flag will auto init)
	auto t_SDCInstance = std::make_shared<SDCInstance>(true);
	
	// Some restriction
	t_SDCInstance->setIP6enabled(false);
	t_SDCInstance->setIP4enabled(true);
	
	// Bind it to interface that matches the internal criteria (usually the first enumerated)
	if(!t_SDCInstance->bindToDefaultNetworkInterface()) {
		DebugOut(DebugOut::Default, CLASS_NAME) << "Failed to bind to default network interface! Exit..." << std::endl;
		return -1;
	}

	SdcControlProvider provider(t_SDCInstance);
	provider.startup();



	std::string temp;
	DebugOut(DebugOut::Default, CLASS_NAME) << "Press key to exit program.";
	std::cin >> temp;

	// Shutdown
	DebugOut(DebugOut::Default, CLASS_NAME) << "Shutdown." << std::endl;
	provider.shutdown();
	SDCLibrary::getInstance().shutdown();

    return 0;
}
