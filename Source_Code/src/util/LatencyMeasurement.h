/*
 * LatencyMeasurement.h
 *
 *  Created on: 06.06.2016
 *      Author: martin
 */

#include <list>
#include <iostream>
#include <fstream>

#ifndef LATENCYMEASUREMENT_H_
#define LATENCYMEASUREMENT_H_

class LatencyMeasurement {
public:
	const std::string T_START_FILENAME = "t0.txt";
	const std::string T_FINISH_FILENAME = "t1.txt";
	const int MAX_NUMBER_LENGTH = 16;

	static LatencyMeasurement& getInstance()
	{
		static LatencyMeasurement instance;

		return instance;
	}

	//@see: http://stackoverflow.com/questions/1008019/c-singleton-design-pattern
	LatencyMeasurement(LatencyMeasurement const&)	= delete;
	void operator=(LatencyMeasurement const &)		= delete;




	void addElementToT0List(long long newValue){
		t0List.push_back(newValue);
	}
	void addElementToT1List(long long newValue){
		t1List.push_back(newValue);
	}



	int getNumOfMeasurements() const {
		return numOfMeasurements;
	}

	void setNumOfMeasurements(int numOfMeasurements) {
		this->numOfMeasurements = numOfMeasurements;
	}

	int getDelayBetweenMeasurements() const {
		return delayBetweenMeasurements;
	}

	void setDelayBetweenMeasurements(int delayBetweenMeasurements) {
		this->delayBetweenMeasurements = delayBetweenMeasurements;
	}

	void printLists()
	{

		std::cout << "toList Number of Elements: " << t0List.size() << std::endl;
		for (std::list<long long>::const_iterator iterator = t0List.begin(), end = t0List.end(); iterator != end; ++iterator) {
			std::cout <<  *iterator << " ";
		}
		std::cout << std::endl;

		std::cout << "toList Number of Elements: " << t1List.size() << std::endl;
		for (std::list<long long>::const_iterator iterator = t1List.begin(), end = t1List.end(); iterator != end; ++iterator) {
			std::cout <<  *iterator << " ";
		}
		std::cout << std::endl;

	}

	void writeEveryMeasurementToFiles(bool append)
	{
		writeToFile(T_START_FILENAME, t0List, append);
		writeToFile(T_FINISH_FILENAME, t1List, append);
	}

	void clearAllMeasurements(){
		t0List.clear();
		t1List.clear();

		writeEveryMeasurementToFiles(false);
	}

	int getDelayBeforeMeasurements() const {
		return delayBeforeMeasurements;
	}

	void setDelayBeforeMeasurements(int delayBeforeMeasurements = 20000) {
		this->delayBeforeMeasurements = delayBeforeMeasurements;
	}

	const std::list<long long>& getT0List() const {
		return t0List;
	}

	void setT0List(const std::list<long long> &t0List) {
		this->t0List = t0List;
	}

	const std::list<long long>& getT1List() const {
		return t1List;
	}

	void setT1List(const std::list<long long> &t1List) {
		this->t1List = t1List;
	}

private:
	LatencyMeasurement(){}
	virtual ~LatencyMeasurement(){}
	static LatencyMeasurement *latencyMeasurement;

	int numOfMeasurements = 10;
	int delayBetweenMeasurements = 223;
	int delayBeforeMeasurements = 20000;

	std::list<long long> t0List;
	std::list<long long> t1List;


	void writeToFile(std::string filename, std::list<long long> list, bool append)
	{
		std::ofstream fileout;
		if(append) {
			fileout.open(filename, std::ios::out | std::ios::app);
		}
		else {
			fileout.open(filename);
		}

		if(!fileout.is_open()){
			std::cerr << "ERROR: could not open file: " << filename << std::endl;
			return;
		}

		std::cout << "Number of Elements: " << list.size() << std::endl;
		for (std::list<long long>::const_iterator iterator = list.begin(), end = list.end(); iterator != end; ++iterator) {
			fileout <<  deleteFirstDigits(std::to_string(*iterator)) << " ";
		}

		fileout.flush();
		fileout.close();
	}

	std::string deleteFirstDigits(std::string input)
	{
		int length = input.length();
		if(length > MAX_NUMBER_LENGTH){
			std::cerr << input << std::endl;
			std::string ret = input.substr(input.length()-MAX_NUMBER_LENGTH+1, input.npos);
			std::cerr << ret << std::endl;
			return ret;
		}
		else{
			return input;
		}

	}
};


#endif /* LATENCYMEASUREMENT_H_ */
