#include <stdlib.h>
#include <string.h>

void waitStartup()
{
	std::string temp;
	std::cout << "Please insert time in [ms] to trigger the start of the measurement. (If input is not a number -> start immediately" << std::endl;
	std::cin >> temp;

	long int waitingTime = strtol(temp.c_str(), NULL, 10);

	if (waitingTime == 0L)
	{
		std::cout << "input was not a number" << std::endl;
	}
	else
	{
		std::cout << "Waiting for " << waitingTime << " ms to trigger measurement start..." << std::endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(waitingTime));
	}

	std::cout << std::endl << "############" << std::endl;
	std::cout << "# Start... #" << std::endl;
	std::cout << "############" << std::endl << std::endl;
}
