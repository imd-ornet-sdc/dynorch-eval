This folder contains the raw data of the analysis being part of the PhD thesis. (Note: DOC - Dynamic Orchestration Component is the same as the German "Orchestrator".)

Content of the folders:
* baseline_without_DOC - contains the timestamp files for the baseline measurements without an orchestrator 
* RPi1_DOC ... RPi4_DOC - contain the timestamp files for the measurements of the DOC running an Raspberry Pi (RPi) 1 … RPi4 with increasing numbers of participants.
