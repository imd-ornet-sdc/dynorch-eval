# SDC Dynamic Orchestration Evaluation

This project deals with a performance evaluation of the concept of service orchestration for dynamic association between control devices and remote/external control operations of devices to be controlled. Everything is based on IEEE 11073 SDC.

It is part of Martin's PhD thesis. Thus, the project contains the source code of the measurement tools as well as the raw data of the measurements evaluated in the PhD thesis.